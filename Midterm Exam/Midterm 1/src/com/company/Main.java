package com.company;


import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        //Random Number
        int min = 1;
        int max = 5;

        //Card Stacks
        CardArrayList stack1 = new CardArrayList(30);
        CardArrayList hand = new CardArrayList(30);
        CardArrayList discardPile = new CardArrayList(30);
        System.out.println("Enter name for 30 cards ");

        //Scanner
        for (int i = 0; i < 30; i++) {
            Scanner scanner = new Scanner(System.in);
            String input = scanner.nextLine();
            stack1.push(new Player(input));
            System.out.println("You entered " + input);
        }
        stack1.printStack();


        int number = stack1.capacity();
        int pileCounter = 30;
        int handcounter = 0;
        int dpcounter = 0;


        while (number > 0) {

            //Draw
            int randomDraw = (int) (Math.random() * (max - min + 1) + min);
            if (pileCounter < randomDraw) {
                randomDraw = pileCounter;
            }
            System.out.println("Draw " + randomDraw);
            for (int i = 0; i != randomDraw; i++) {

                Player draw = stack1.peek();
                stack1.pop();
                hand.push(new Player(draw.getCardName()));
                pileCounter--;
                handcounter++;

            }
            //Discard
            int randomDiscard = (int) (Math.random() * (max - min + 1) + min);
            if (handcounter < randomDiscard) {

                randomDiscard = handcounter;
                handcounter -= randomDiscard;
            }
            System.out.println("Discard " + randomDiscard);
            for (int i = 0; i < randomDiscard; i++) {

                Player discard = hand.peek();
                hand.pop();
                discardPile.push(new Player(discard.getCardName()));
                dpcounter++;
                handcounter--;
            }
            //Return from Discard
            int randomRedraw = (int) (Math.random() * (max - min + 1) + min);
            if (dpcounter < randomRedraw) {

                randomRedraw = dpcounter;
            }
            System.out.println("Redraw " + randomRedraw);
            for (int i = 0; i < randomRedraw; i++) {

                Player returnFrom = discardPile.peek();
                discardPile.pop();
                hand.push(new Player(returnFrom.getCardName()));
                handcounter++;
                dpcounter--;
            }

            //Print out
            System.out.println("Draw Pile cards");
            stack1.printStack();

            System.out.println("Hand cards");
            hand.printStack();

            System.out.println("Discard Pile cards");
            discardPile.printStack();
            int stuff = number - randomDraw;
            number = stuff;


        }
        System.out.println("There no more cards left in the pile");
    }
}
