package com.company;

public class Player {

    private String cardName;

    @Override
    public String toString() {
        return
                " CardName='" + cardName + '\'';
    }

    public Player(String name) {

        this.cardName = name;

    }


    public String getCardName() {
        return cardName;
    }

    public void setCardName(String cardName) {
        this.cardName = cardName;
    }


}

