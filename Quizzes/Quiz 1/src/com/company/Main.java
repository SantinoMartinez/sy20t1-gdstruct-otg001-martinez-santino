package com.company;


public class Main {

    public static void main(String[] args) {

        int[] numbers = new int[5];
        int[] numbers2 = new int[5];
        int[] numbers3 = new int[5];

        numbers[0] = 27;
        numbers[1] = 4;
        numbers[2] = 45;
        numbers[3] = 100;
        numbers[4] = 12;

        numbers2[0] = 53;
        numbers2[1] = 210;
        numbers2[2] = 1;
        numbers2[3] = 77;
        numbers2[4] = 150;

        numbers3[0] = 15;
        numbers3[1] = 84;
        numbers3[2] = 11;
        numbers3[3] = 44;
        numbers3[4] = 12;


        System.out.println("The array for bubble sort is ");
        printArrayElements(numbers);
        bubbleSorting(numbers);
        System.out.println("The array for bubble sort in descending order is ");
        printArrayElements(numbers);
        System.out.println("The array for selection sort is ");
        printArrayElements(numbers2);
        System.out.println("The array for selection sort in descending order is ");
        selectionSorting(numbers2);
        printArrayElements(numbers2);
        System.out.println("The array for selection sort 2 is ");
        printArrayElements(numbers3);
        System.out.println("The array for selection sort 2 updated is ");
        selectionSorting2(numbers3);
        printArrayElements(numbers3);
    }

    private static void printArrayElements(int[] Numbers) {
        for (int i : Numbers) {
            System.out.println(i);
        }
    }

    private static void bubbleSorting(int[] array) {
        for (int lastSortedItem = array.length - 1; lastSortedItem > 0; lastSortedItem--) {
            for (int j = 0; j < lastSortedItem; j++) {
                if (array[j] < array[j + 1]) {
                    int sub = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = sub;
                }

            }
        }

    }

    private static void selectionSorting(int[] array) {
        for (int lastSortedItem = array.length - 1; lastSortedItem > 0; lastSortedItem--) {
            int item = 0;

            for (int j = 0; j <= lastSortedItem; j++) {
                if (array[j] < array[item]) {
                    item = j;
                }
            }

            int sub = array[lastSortedItem];
            array[lastSortedItem] = array[item];
            array[item] = sub;


        }


    }

    private static void selectionSorting2(int[] array) {
        for (int lastSortedItem = 0; lastSortedItem < array.length - 1; lastSortedItem++) {
            int item = array.length - 1;

            for (int j = array.length - 1; j >= lastSortedItem; j--) {
                if (array[j] < array[item]) {
                    item = j;
                }
            }

            int sub = array[lastSortedItem];
            array[lastSortedItem] = array[item];
            array[item] = sub;


        }
    }
}