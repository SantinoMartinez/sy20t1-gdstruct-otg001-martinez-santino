package com.company;

public class Tree {

    private Node root;

    public void insert(int value)
    {
        if (root == null)
        {
            root = new Node(value);
        }
        else
        {
            root.insert(value);
        }

    }


    public void transverseInOrder()
    {
        if (root != null)
        {
            root.traverseInOrder();
        }
    }
    public void transverseInDescendingOrder()
    {
        if (root != null)
        {
            root.traverseInDescendingOrder();
        }
    }

    public Node get(int value)
    {
        if (root != null)
        {
            return root.get(value);
        }
        return null;
    }
    public Node getMin()
    {
        if (root != null)
        {
             return root.getMin();
    } else {
            return null;
        }
    }
    public Node getMax()
    {
        if (root != null)
        {
            return root.getMax();
        } else {
            return null;
        }

    }


}
