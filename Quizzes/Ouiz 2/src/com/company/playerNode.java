package com.company;
public class playerNode {
    private player Player;
    private playerNode nextPlayer;
private playerNode previousPlayer;

    public playerNode(player player) {
        this.Player = player;
    }

    public playerNode getPreviousPlayer() {
        return previousPlayer;
    }

    public void setPreviousPlayer(playerNode previousPlayer) {
        this.previousPlayer = previousPlayer;
    }

    public player getPlayer() {
        return Player;
    }
    public void setPlayer(player player) {
        Player = player;
    }
    public playerNode getNextPlayer() {
        return nextPlayer;
    }
    public void setNextPlayer(playerNode nextPlayer) {
        this.nextPlayer = nextPlayer;
    }

    @Override
    public String toString() {
        return "playerNode{" +
                "Player=" + Player +
                ", nextPlayer=" + nextPlayer +
                ", previousPlayer=" + previousPlayer +
                '}';
    }
}
