package com.company;
import java.util.Random;
public class Main {
// Name: Two Hit Search
    //Requirements: Data has to be sorted from least to greatest
    //Steps:
    //The program will select 2 random numbers from the array
    // Once the two have been selected it will either choose the one that either equal or closest to the targeted value
    // The chosen number will be used as a basis and will ,similar to binary, either add / subtract from the basis
    //Once the number has been found it will show chosen
    //If number is not there it will show nothing
    public static void main(String[] args) {

        int numbers[] = {-90, -30, 10, 40, 50, 60, 70};


        System.out.println(randomSearch(numbers, -30));

    }

    public static int randomSearch(int[] input, int value) {
        Random rand = new Random(); //instance of random class
        int highest = input.length - 1; //generate random values from 0-number of items;
        int random = rand.nextInt(highest);
        int random2 = rand.nextInt(highest);
        System.out.println("Random number space 0 to " + (highest-1) + " : "+ random);
        System.out.println("Random number space 0 to " + (highest-1) + " : "+ random2);
        int search = 0;
        //Equal
        if(input[random] == value)
        {
            search = random;
        } else if(input[random2] == value)
        {
            search = random2;
        } else if(input[random] > value && input[random2] > value) // both are greater
     {
         int answer = input[random] - value;
         int answer2 = input[random2] - value;
         if(answer == answer2)
         {
             search = random;
         }else if(answer < answer2)
         {
             search = random;
         }else if(answer > answer2)
         {
             search = random2;
         }
     } else if(input[random] < value && input[random2] < value) //both are less
     {

         int answer = value - input[random];
         int answer2 =  value - input[random2];

         if (answer == answer2) {
             search = random;
         } else if (answer < answer2) {
             search = random;
         } else if (answer > answer2) {
             search = random2;
         }
     }else if(input[random] < value && input[random2] > value) // first is greater second is lesser
     {

         int answer = value - input[random];
         int answer2 =  input[random2] - value;

         if (answer == answer2) {
             search = random;
         } else if (answer < answer2) {
             search = random;
         } else if (answer > answer2) {
             search = random2;
         }
     }else if(input[random] > value && input[random2] < value) // first is lesser , second is greater
     {

         int answer = input[random] - value;
         int answer2 =  value - input[random2];

         if (answer == answer2) {
             search = random;
         } else if (answer < answer2) {
             search = random;
         } else if (answer > answer2) {
             search = random2;
         }
     }


        while(search > -1 || search < input.length- 1)
        {
            if(input[search] == value)
            {
                return search;
            } else if(input[search] > value)
            {
                search = search -1;
            }else if(input[search] < value)
            {
                search = search + 1;
            }

        }

        return -1;
    }

}
