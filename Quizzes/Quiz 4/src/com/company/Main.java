package com.company;

public class Main {

    public static void main(String[] args) {

        Player burger = new Player(44, "Tamm y", 1);
        Player fries = new Player(590, "Mike y", 2);
        Player soda = new Player(92, "123456789", 3);

        SimpleHashTable hashtable = new SimpleHashTable();

        hashtable.put(burger.getName(), burger);
        hashtable.put(fries.getName(), fries);
        hashtable.put(soda.getName(), soda);


        System.out.println(hashtable.get("Mike y"));
        hashtable.remove("123456789");
        System.out.println(hashtable.get("123456789"));
        hashtable.printHashtable();
    }

}
