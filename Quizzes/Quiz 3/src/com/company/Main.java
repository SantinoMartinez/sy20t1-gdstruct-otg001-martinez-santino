package com.company;


import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        // write your code here
        int counter1 = 0;
        int counter2 = 0;
        int min = 1;
        int max = 7;

        int game = 1;
        int capacity = 0;
        //Queue
        ArrayQueue queue = new ArrayQueue(14);
        System.out.println("Game 1" );
        while (game != 11) {

            // How many players to enqueue
            int enqueuePlayer = (int) (Math.random() * (max - min + 1) + min);
            int numberX = enqueuePlayer;
            System.out.println("Enter " + numberX + " players names");
            // Adding Names
            for (int i = 0; i < numberX; i++) {
                counter1++;
                counter2++;
                capacity++;
                Scanner scanner = new Scanner(System.in);
                String input2 = scanner.nextLine();
                queue.enqueue(new Player(counter1, input2, counter2));
                System.out.println("You entered " + input2);
            }
            //Print list
            queue.printQueue();
            //Delete + Print list
            if (capacity > 5) {
                System.out.println("You remove 5" );
                for (int i = 0; i < 5; i++)
                {
                    System.out.println("You remove " + queue.remove());
                    capacity--;
                }
                System.out.println("Remaining List: ");
                queue.printQueue();
                game++;
                System.out.println("Press Enter ");
                new java.util.Scanner(System.in).nextLine(); // System pause

                System.out.println("Game " + game);
            }
        }
        System.out.println("You completed all your games ");
    }
}
